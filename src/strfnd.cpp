#include <iostream>
#include "strfnd.h"

#define IS_SPACE(C) (C == ' ' || C == '\t' || C == '\r' || C == '\n')

std::string trim(const std::string &s)
{
	size_t start = 0;
	size_t end = s.size() - 1;
	while (end - start) {
	  const char c = s[start];
	  if (!IS_SPACE(c))
	    break;
	  ++start;
	}
	while (end - start) {
	  const char c = s[end];
	  if (!IS_SPACE(c))
	    break;
	  --end;
	}
	if (end == start)
	  return "";
	return s.substr(start, end - start + 1);
}
