/************************************************************************
* Minetest-c55
* Copyright (C) 2012 celeron55, Perttu Ahola <celeron55@gmail.com>
*
* sound.h
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*
* License updated from GPLv2 or later to GPLv3 or later by Lisa Milne
* for Voxelands.
************************************************************************/

#ifndef SOUND_HEADER
#define SOUND_HEADER

#if defined(_MSC_VER)
	#include <al.h>
	#include <alc.h>
	#include <alext.h>
#elif defined(__APPLE__)
	#include <OpenAL/al.h>
	#include <OpenAL/alc.h>
#else
	#include <AL/al.h>
	#include <AL/alc.h>
	#include <AL/alext.h>
#endif

#include "common_irrlicht.h"
#include <string>
#include <vector>
#include <set>
#include <map>
#include "mapnode.h"

class SoundBuffer;
class PlayingSound;


class SoundManager
{
private:
	ALCdevice *m_device;
	ALCcontext *m_context;
	bool m_can_vorbis;
	int m_next_id;
	int m_music_id;
	int m_music_last_id;
	std::map<std::string, std::vector<SoundBuffer*> > m_buffers;
	std::map<int, PlayingSound*> m_sounds_playing;
	std::map<std::string, int> m_indexes;
	v3f m_listener_pos;
	JMutex m_mutex;

public:
	SoundManager();
	~SoundManager();

public:
	void addBuffer(const std::string &name, SoundBuffer *buf);
	SoundBuffer* getBuffer(const std::string &name);
	PlayingSound* createPlayingSound(SoundBuffer *buf, bool loop);
	PlayingSound* createPlayingSoundAt(SoundBuffer *buf, bool loop, v3f pos, float gain, bool queue);
	int playSoundRaw(SoundBuffer *buf, bool loop);
	int playSoundRawAt(SoundBuffer *buf, bool loop, v3f pos, float gain, bool queue);
	void deleteSound(int id);
	void maintain(float dtime);
	bool loadSound(const std::string &name, const std::string &filepath, float gain=1.0);
	void updateListener(v3f pos, v3f vel, v3f at, v3f up);
	void setListenerGain(float gain);
	int playSound(const std::string &name, bool loop);
	int playSoundAt(const std::string &name, bool loop, v3f pos, float gain=1.0, bool queue=false);
	void stopSound(int id);
	bool soundExists(int sound);
	bool playMusic(const std::string &name, bool loop);
	void stopMusic();
	void updateSoundPosition(int id, v3f pos);
};

SoundManager *createSoundManager();

SoundManager *createSoundManager();
void init_sounds(SoundManager *sound);

class Map;

extern SoundManager *g_sound; // FIXME: write a real singleton class

void sound_playStep(Map *map, v3f pos, int foot, float gain=1.0);
void sound_playDig(content_t c, v3f pos);

#endif
